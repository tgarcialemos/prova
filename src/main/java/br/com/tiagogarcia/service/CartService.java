package br.com.tiagogarcia.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import br.com.tiagogarcia.domain.Article;
import br.com.tiagogarcia.domain.Cart;
import br.com.tiagogarcia.domain.Discount;
import br.com.tiagogarcia.domain.EligibleVolume;
import br.com.tiagogarcia.domain.ResumeCart;

@Service
public class CartService {

	public List<ResumeCart> calculateCarts(List<Article> aticles, List<Cart> listCarts) {

		List<ResumeCart> listaResume = new ArrayList<ResumeCart>();

		for (Cart cart : listCarts) {

			ResumeCart resumeCart = new ResumeCart();
			resumeCart.setId(cart.getId());
			resumeCart.setTotal(cart.getTotal(aticles));
			listaResume.add(resumeCart);
		}

		return listaResume;
	}
	
	public List<ResumeCart> calculateCartsWithDiscount(List<Article> aticles, List<Cart> listCarts, List<Discount> discounts) {

		List<ResumeCart> listaResume = new ArrayList<ResumeCart>();

		for (Cart cart : listCarts) {

			ResumeCart resumeCart = new ResumeCart();
			resumeCart.setId(cart.getId());
			resumeCart.setTotal(cart.getTotalWithDiscount(aticles, discounts));
			listaResume.add(resumeCart);
		}

		return listaResume;
	}
	
	
	public List<ResumeCart> calculateFee(List<EligibleVolume> delivery_fees, List<ResumeCart> listCart) {
		
		for (ResumeCart resumeCart : listCart) {
			
			Double fee = getFee(resumeCart.getTotal(), delivery_fees);
			resumeCart.setTotal(resumeCart.getTotal() + fee);
			
		}
		
		return listCart;
	}

	private Double getFee(Double total, List<EligibleVolume> delivery_fees) {
		
		for (EligibleVolume eligibleVolume : delivery_fees) {
			if(eligibleVolume.isEligible(total)){
				return eligibleVolume.getPrice();
			}
		}
		
		return new Double(0);
	}

}
