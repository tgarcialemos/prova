package br.com.tiagogarcia.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.tiagogarcia.domain.Request;
import br.com.tiagogarcia.domain.Response;
import br.com.tiagogarcia.domain.ResumeCart;
import br.com.tiagogarcia.service.CartService;

@RestController
@RequestMapping(value = "/cart")
public class CartController {

	@Autowired
	private CartService cartService;

	@RequestMapping(value = "/level1", method = RequestMethod.GET)
	public ResponseEntity<Response> level1(@RequestBody Request request) {

		List<ResumeCart> listCart = null;
		Response response = new Response();

		try {

			listCart = cartService.calculateCarts(request.getArticles(), request.getCarts());
			response.setCarts(listCart);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return new ResponseEntity<Response>(response, HttpStatus.OK);

	}

	@RequestMapping(value = "/level2", method = RequestMethod.GET)
	public ResponseEntity<Response> level2(@RequestBody Request request) {

		Response response = new Response();

		List<ResumeCart> listCart = null;

		try {
			listCart = cartService.calculateCarts(request.getArticles(), request.getCarts());
			listCart = cartService.calculateFee(request.getDelivery_fees(), listCart);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return new ResponseEntity<Response>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/level3", method = RequestMethod.GET)
	public ResponseEntity<Response> level3(@RequestBody Request request) {

		Response response = new Response();

		List<ResumeCart> listCart = null;

		try {
			listCart = cartService.calculateCartsWithDiscount(request.getArticles(), request.getCarts(),
					request.getDiscounts());
			listCart = cartService.calculateFee(request.getDelivery_fees(), listCart);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return new ResponseEntity<Response>(response, HttpStatus.OK);
	}

}
