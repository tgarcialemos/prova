package br.com.tiagogarcia.domain;

public class Discount {

	private Long article_id;

	private String type;

	private Double value;

	public Long getArticle_id() {
		return article_id;
	}

	public void setArticle_id(Long article_id) {
		this.article_id = article_id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public Double calculateDiscount(Double articleValue) {

		if (this.type == "amount") {
			return articleValue - this.value;

		} else if (this.type == "percentage") {

			return articleValue - articleValue * this.value / 100;
		}

		return articleValue;
	}

}
