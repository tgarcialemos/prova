package br.com.tiagogarcia.domain;

import java.util.List;

public class Request {

	private List<Article> articles;

	private List<Cart> carts;

	private List<EligibleVolume> delivery_fees;

	private List<Discount> discounts;

	public List<Discount> getDiscounts() {
		return discounts;
	}

	public void setDiscounts(List<Discount> discounts) {
		this.discounts = discounts;
	}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> aticles) {
		this.articles = aticles;
	}

	public List<Cart> getCarts() {
		return carts;
	}

	public void setCarts(List<Cart> carts) {
		this.carts = carts;
	}

	public List<EligibleVolume> getDelivery_fees() {
		return delivery_fees;
	}

	public void setDelivery_fees(List<EligibleVolume> delivery_fees) {
		this.delivery_fees = delivery_fees;
	}

}
