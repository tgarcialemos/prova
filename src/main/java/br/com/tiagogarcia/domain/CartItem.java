package br.com.tiagogarcia.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CartItem {
	
	@JsonProperty("article_id")
	private Long articleId;
	
	private Integer quantity;
	
	public Long getArticleId() {
		return articleId;
	}

	public void setArticleId(Long articleId) {
		this.articleId = articleId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	

}
