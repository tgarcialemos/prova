package br.com.tiagogarcia.domain;

public class EligibleVolume {

	private Double min_price;

	private Double max_price;

	private Double price;

	public Double getMin_price() {
		return min_price;
	}

	public void setMin_price(Double min_price) {
		this.min_price = min_price;
	}

	public Double getMax_price() {
		return max_price;
	}

	public void setMax_price(Double max_price) {
		this.max_price = max_price;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Boolean isEligible(Double cartPrice) {

		if (this.max_price == null) {
			if (cartPrice > min_price) {
				return true;
			}
		} else {
			if (cartPrice > min_price && cartPrice < max_price) {
				return true;
			}
		}

		return false;

	}

}
