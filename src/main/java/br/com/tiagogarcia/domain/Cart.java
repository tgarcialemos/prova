package br.com.tiagogarcia.domain;

import java.util.ArrayList;
import java.util.List;

public class Cart {

	private Long id;
	
	private List<CartItem> items;
	
	public Cart() {
		// TODO Auto-generated constructor stub
		items = new ArrayList<CartItem>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<CartItem> getItems() {
		return items;
	}

	public void setItems(List<CartItem> items) {
		this.items = items;
	}
	
	private Article findArticle(List<Article> articles, long id) {

		for (Article article : articles) {
			if (article.getId().longValue() == id) {
				return article;
			}
		}

		return null;
	}
	
	private Discount findDiscount(List<Discount> discounts, long id) {

		for (Discount discount : discounts) {
			if (discount.getArticle_id().longValue() == id) {
				return discount;
			}
		}

		return null;
	}
	
	public Double getTotal(List<Article> articles){
		Double total = new Double(0);
		for (CartItem item : this.getItems()) {
			Article article = findArticle(articles, item.getArticleId());
			total = total + article.getPrice() * item.getQuantity();
		}
		return total;
	}
	
	public Double getTotalWithDiscount(List<Article> articles, List<Discount> discounts){
		Double total = new Double(0);
		for (CartItem item : this.getItems()) {
			Article article = findArticle(articles, item.getArticleId());
			Discount discount = findDiscount(discounts, item.getArticleId());
			total = total + discount.calculateDiscount(article.getPrice()) * item.getQuantity() ;
		}
						
		return total;
	}
	
}
