package br.com.tiagogarcia.domain;

import java.util.List;

public class Response {

	private List<ResumeCart> carts;

	public List<ResumeCart> getCarts() {
		return carts;
	}

	public void setCarts(List<ResumeCart> carts) {
		this.carts = carts;
	}

}